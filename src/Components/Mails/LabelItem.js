/* IMPORTS */

// Library imports
import React from 'react';


// Local file imports
import './mail.css';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../../../node_modules/font-awesome/css/font-awesome.min.css';


// Definition of one label, and properties when clicking on it
class LabelItem extends React.Component {

  handleClick(){
    console.log('handleClick '+this.props.id);
    this.props.onClick(this.props.id);
  }

  render(){
    return (
        <li className="list-group-item justify-content-between" onClick={this.handleClick.bind(this)}>
          {this.props.label.name}
          {/* Number of mails per category : <span className="badge badge-default badge-pill">{this.props.label.emailNumber}</span>*/}
        </li>
    )
  }
}

export default LabelItem;
